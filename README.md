Nejdřív jsem v Excelu manuálně upravil soubor `ALFA_logbook_20211202.xlsx` a uložil ho do stejného adresáře jako `data.csv`. Původní data včetně fotek jsou v adresáři `data_original` na Drivu. 

Notebook `preprocessing.ipynb` jsem spouštěl u sebe na PC. Čistí data, hledá a přejmenovává fotky aby se s nimi dalo normálně pracovat. Výstup tohoto notebooku je na Drivu v adresáři `data`. V tomto adresáři jsou i anotace k fotkám.

Notebook `MVI.ipynb` jsem pouštěl v Collabu, ale ty modely jsou tak jednoduché, že se to nechá natrénovat i na CPU. Váhy MLP a TabNetu jsou na Drivu v adresáři `weights`.

[Přístup na Google Drive](https://drive.google.com/drive/folders/1FGnHFy-6WToZ0LyeY-G4BBsh3F-HArki?usp=sharing)